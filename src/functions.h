#pragma once
#include "chess.h"
#include <iostream>
#include <vector>
#include <cstddef>
#include <cstdlib>        

#ifndef FUNCTIONS_LIB_H
#define FUNCTIONS_LIB_H

bool concede_game(int turns);
auto get_move_from_user(std::string input) -> std::vector<int>;

#endif
