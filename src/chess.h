#pragma once
#include <string>
#include <vector>
#include<iostream>
#include <fstream>
#include <cstdlib>
#include <climits>
#include "../hps/hps.h"

class Chess{
    protected:
        std::vector<std::string> print_chars; // For displaying the pieces
        std::vector<std::vector<int>> board; //(8x8 matrix)
        std::vector<std::vector<int>> moves; //(4xN)
        std::vector<bool> rook_has_moved; //For checking if castling is allowed
        std::vector<bool> king_has_moved; //For checking if castling is allowed
        int turns; //number of turns
        bool game_completed;
    
    public:
        bool w_checked;
        bool b_checked;
        bool b_check_mate;
        bool w_check_mate;
        struct Position
        {
          int row;
          int column;
        };
        struct Attacked
        {
          char color;
          std::vector<Position> attackers;
          bool targeted = false;
        };
        
        Chess();
        void setup_board();
        void reset_board();
        void reset_from_moves();
        void draw_chessboard();
        bool checkCheckmate();
        Position find_white_king();
        Position find_black_king();
        bool is_white_checked();
        bool is_black_checked();
        bool canBeBlocked(Position king, Position threat);
        bool isChecked(Position king_pos);
        Attacked checkTargeted(Position pos);
        bool isCompleted();
        void incrementTurns();
        void king_checked();
        bool your_turn(std::vector<int> pos);
        bool move(std::vector<int> pos);
        bool is_knight(std::vector<int> move);
        bool check_blocked(std::vector<int> pos);
        bool move_piece(std::vector<int> pos);
        bool move_pawn(std::vector<int> pos, int color);
        bool move_rook(std::vector<int> pos);
        bool move_knight(std::vector<int> pos);
        bool move_bishop(std::vector<int> pos);
        bool move_queen(std::vector<int> pos);
        bool move_king(std::vector<int> pos);
        bool validate_move(std::vector<int> pos);
        void perform_move(std::vector<int> move);
        bool perform_castling(std::vector<int> move);
        void pawn_promotion(std::vector<int> pos);
        bool positionOccupied(int row, int column);
        int getTurns();
        int getValuePosition(int row, int column);
        void print_moves();
        void save_board();
        void load_board();  
};
