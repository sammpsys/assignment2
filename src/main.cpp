#include"functions.h"
#include"chess.h"

//Author Hannes and Thorbjoern
int main(){
    std::cout << "Welcome to Chess."<<std::endl;
    std::cout << "New game [n], Load game [l], Save game [s], Quit game [q], Print all moves [p]"<<std::endl;
    Chess chessGame;
    chessGame.setup_board();   
    bool keep_playing=true;
    bool valid_move;
    std::string input;
    std::vector<int> move;
    while(keep_playing)
    {
        // Get input from user
        chessGame.king_checked();
        std::cout << "Type here: ";
        getline(std::cin, input);
        if (input.length() != 1)
        {
            valid_move = false;
            do{
                move = get_move_from_user(input);
                valid_move = chessGame.move(move);
                if(!valid_move)
                {
                    std::cout<<"Invalid move"<<std::endl;
                    getline(std::cin, input);
                }
            } while(!valid_move);
            chessGame.draw_chessboard();         
        }
        else
        {
            switch (input[0])
            {
                case 'P':
                case 'p':
                {
                    chessGame.print_moves();
                }
                break;

                case 'N':
                case 'n':
                {
                    chessGame.reset_board();
                    chessGame.draw_chessboard();
                }
                break;

                case 'Q':
                case 'q':
                {
                    keep_playing = concede_game(chessGame.getTurns());
                }
                break;

                case 'S':
                case 's':
                {
                    chessGame.save_board();
                }
                break;

                case 'L':
                case 'l':
                {
                    chessGame.load_board();
                    chessGame.print_moves();
                    chessGame.reset_from_moves();
                    chessGame.draw_chessboard();
                }
                break;

                default:
                {
                    std::cout << "Option does not exist\n\n";
                }
                break;
            }
        }
    }
    return 0;
}