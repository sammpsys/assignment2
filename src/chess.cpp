#include "chess.h"

//constructor chess class
// Author Thorbjoern
Chess::Chess(){     
    std::vector<std::string> display_chars = {"\u2659","\u2656","\u2658","\u2657","\u2655","\u2654","\u265F","\u265C","\u265E","\u265D","\u265B","\u265A"};
    this -> print_chars = display_chars;
    std::vector<bool> rook_has_moved = {false,false,false,false}; //For checking if castling is allowed
    std::vector<bool> king_has_moved = {false,false}; //For checking if castling is allowed
    this -> rook_has_moved = rook_has_moved;
    this -> king_has_moved = king_has_moved;
    turns = 0;
};

void Chess::incrementTurns(){
    turns++;
}

int Chess::getTurns()
{
    return turns;
}


// Check if the piece can move to the target tile
//Author Sam
bool Chess::validate_move(std::vector<int> pos){       //validate if square is empty and not of the same color
    int old_row=pos[0];       //pos is in format d2 d4, letter is column and number is row
    int old_col=pos[1];       // we access board in [row][column] format
    int new_row=pos[2];       // The x, y swap is done in the receive
    int new_col=pos[3];
    if (board[old_row][old_col] !=0)
    {
        if (board[new_row][new_col] != 0){        //check if square is empty
            if ((board[new_row][new_col] > 0 && board[new_row][new_col] < 7) && (board[old_row][old_col] > 0 && board[old_row][old_col] < 7)){
                return 0;
            }
            else if ((board[new_row][new_col] > 6 && board[new_row][new_col] < 13) && (board[old_row][old_col] > 6 && board[old_row][old_col] < 13)){
                return 0;
            }
            else{
                return 1;
            }
        }
        else{
            return 1;
        }
    }
    else
    {
        return 0;
    }
}

// Update the board state with the move
// Author Thorbjoern
void Chess::perform_move(std::vector<int> move)
{
    board[move[2]][move[3]] = board[move[0]][move[1]];
    board[move[0]][move[1]] = 0;
    moves.push_back(move);
    incrementTurns();
}

// Check if the input move is a castling move and if it is a valid castling
// Author Thorbjoern
bool Chess::perform_castling(std::vector<int> move)
{
    bool isCastling_move = false;
    // Castling is called by first piece = king location, second piece = rook location.
    if((move[0]==0) && (move[1]==4) && (move[2]==0) && (move[3]==0) && (board[0][1]==0) && (board[0][2]==0) && (board[0][3]==0))
    {
        // Long white castling
        board[0][2] = 6;
        board[0][3] = 2;
        board[0][4] = 0;
        board[0][0] = 0;
        isCastling_move = true;
    }
    else if((move[0]==0) && (move[1]==4) && (move[2]==0) && (move[3]==7) && (board[0][5]==0) && (board[0][5]==0)) 
    {
        // Short white castling
        board[0][6] = 6;
        board[0][5] = 2;
        board[0][4] = 0;
        board[0][7] = 0;
        isCastling_move = true;
    }
    if((move[0]==7) && (move[1]==4) && (move[2]==7) && (move[3]==0) && (board[7][1]==0) && (board[7][2]==0) && (board[7][3]==0))
    {
        // Long black castling
        board[7][2] = 6;
        board[7][3] = 2;
        board[7][4] = 0;
        board[7][0] = 0;
        isCastling_move = true;
    }
    else if((move[0]==7) && (move[1]==4) && (move[2]==7) && (move[3]==7) && (board[7][5]==0) && (board[7][5]==0)) 
    {
        // Short black castling
        board[7][6] = 6;
        board[7][5] = 2;
        board[7][4] = 0;
        board[7][7] = 0;
        isCastling_move = true;
    }
    if(isCastling_move)
    {
        moves.push_back(move);
        incrementTurns();
    }
    return isCastling_move;
}

// Check if we are moving a knight
// Author Thorbjoern
bool Chess::is_knight(std::vector<int> move)
{
    if(board[move[0]][move[1]]==3 || board[move[0]][move[1]]==9)
    {
        return true;
    }
    else
    {
        return false;
    }
}

// Author Thorbjoern
// Display if the active players king is in check
void Chess::king_checked()
{
    bool is_king_checked = false;
    if((turns%2)==0)
    {
        is_king_checked = is_white_checked();
    }
    else
    {
        is_king_checked = is_black_checked();
        
    }
    if(is_king_checked)
    {
        std::cout<<"Your king is in check"<<std::endl;
    }
}

// Wrapper for all the checks we need to perform for doing a move
// Author Thorbjoern
bool Chess::move(std::vector<int> pos)
{
    bool king_checked;
    // Check first if we are trying to castle the king
    if(perform_castling(pos))
    {
        return true;
    }
    else
    {
        if((is_knight(pos) || check_blocked(pos)) && validate_move(pos) && move_piece(pos) && your_turn(pos))
        {
            perform_move(pos);
            if((turns%2)==1)
            {
                king_checked = is_white_checked();
            }
            else
            {
                king_checked = is_black_checked();
                
            }
            if(king_checked) // Undo the move if the king is still in check.
            {
                moves.pop_back();
                reset_from_moves();
                std::cout<<"Your king is still in check"<<std::endl;
                return false;
            }
            pawn_promotion(pos);
            return true;
        }
        else
        {
            return false;
        }
    }
}

// Author Sam, refactored and integrated by Thorbjoern
bool Chess::move_pawn(std::vector<int> pos, int color)
{
    int old_row=pos[0];       
    int old_col=pos[1];       
    int new_row=pos[2];
    int new_col=pos[3];
    int turns=getTurns();
    if (turns>1){
        std::vector<int> last_move=moves.back();    //enpassant check
        int enpass_row=last_move[0];    //enpassant check  start row of last move
        int enpass_col=last_move[1];    //enpassant check  start column of last move
        int enpass_row2=last_move[2];    //enpassant check end row of last move
        int enpass_col2=last_move[3];    //enpassant check end column of last move

        //check  white en passant
        if (color==0 && abs(enpass_row2-enpass_row)==2  && abs(enpass_col2-old_col)==1 && (enpass_row2==old_row) && board[enpass_row2][enpass_col2]==7)
        {
        return true;
        }
            //check  black en passant
        else if (color==1 && abs(enpass_row2-enpass_row)==2  && abs(enpass_col2-old_col)==1 && (enpass_row2==old_row) && board[enpass_row2][enpass_col2]==1)
        {
        return true;
        }
    }
    // White pawn move 
    if (color==0 && (new_row-old_row)==1 && (new_col-old_col)==0 && board[pos[2]][pos[3]]==0){
        return true;
    }
    //double start move for white pawns             
    else if (color==0 && (new_row-old_row)==2 && (new_col-old_col)==0 && old_row==1 && board[pos[2]][pos[3]]==0){
        return true;
    }
    // Black pawn move 
    if (color==1 && (new_row-old_row)==-1 && (new_col-old_col)==0 && board[pos[2]][pos[3]]==0){
        return true;
    }
    //double start move for black pawns             
    else if (color==1 && (new_row-old_row)==-2 && (new_col-old_col)==0 && old_row==6 && board[pos[2]][pos[3]]==0){
        return true;
    }
    // white pawn attack
    else if (color==0 && (new_row-old_row)==1 && abs(new_col-old_col)==1 && board[new_row][new_col]>6){
        return true;
    }
    // black pawn attack
    else if (color==1 && (new_row-old_row)==-1 && abs(new_col-old_col)==1 && board[new_row][new_col]>0 && board[new_row][new_col]<7){
        return true;
    }
    else{
        return false;
    }
}

// Check if we can promote a pawn, ask the user for the desired piece.
// Author Thorbjoern
void Chess::pawn_promotion(std::vector<int> pos)
{
    // Check if we moved a pawn to either side of the board
    if((board[pos[2]][pos[3]]%6)==1 && (pos[2]==0 || pos[2]==7))
    {
        std::cout<<"Choose new piece 2:Rook, 3:Knight, 4:Bishop, 5:Queen (Default): ";
        std::string input; 
        getline(std::cin, input);
        if((int(input[0])-49)>=2 && (int(input[0])-49)<6)
        {
        }
        else
        {
            input = "5";
        }
        board[pos[2]][pos[3]] = board[pos[2]][pos[3]] + int(input[0] - '0');
    }
}

// Author Sam, refactored and integrated by Thorbjoern
bool Chess::move_rook(std::vector<int> pos)
{
    int old_row=pos[0];       
    int old_col=pos[1];       
    int new_row=pos[2];
    int new_col=pos[3];  
    //vertical move
    if (abs(new_row-old_row)>0 && abs(new_col-old_col)==0){ 
        return true;
    }
    // horizontal move
    else if (abs(new_row-old_row)==0 && abs(new_col-old_col)>0){ 
        return true;
    }
    else{
        return false;
    }
}

// Author Sam, refactored and integrated by Thorbjoern
bool Chess::move_bishop(std::vector<int> pos)
{
    int old_row=pos[0];       
    int old_col=pos[1];       
    int new_row=pos[2];
    int new_col=pos[3];
    if (abs(new_row-old_row)==abs(new_col-old_col)){
        return true;
    }

    else{
        return false;
    }
}

// Author Sam, refactored and integrated by Thorbjoern
bool Chess::move_knight(std::vector<int> pos)
{
    int old_row=pos[0];       
    int old_col=pos[1];       
    int new_row=pos[2];
    int new_col=pos[3];
    if (abs(new_row-old_row)==1 && abs(new_col-old_col)==2) {
        return true;
    }
    else if (abs(new_row-old_row)==2 && abs(new_col-old_col)==1) {
        return true;
    }
    else{
        return false;
    }
}

// Author Sam, refactored and integrated by Thorbjoern
bool Chess::move_queen(std::vector<int> pos)
{
    int old_row=pos[0];       
    int old_col=pos[1];       
    int new_row=pos[2];
    int new_col=pos[3];
    //move queen vertically
    if (abs(new_row-old_row)>=1 && abs(new_col-old_col)==0){
        return true;
    } //move queen horizontally
    else if (abs(new_row-old_row)==0 && abs(new_col-old_col)>=1){
        return true;
    } //move queen diagonally
    else if (abs(new_row-old_row)==abs(new_col-old_col)){
        return true;
    } 
    else{
        return false;
    }
}

// Author Sam, refactored and integrated by Thorbjoern
bool Chess::move_king(std::vector<int> pos)
{
    int old_row=pos[0];       
    int old_col=pos[1];       
    int new_row=pos[2];
    int new_col=pos[3];
    //move king vertically
    if (abs(new_row-old_row)==1 && abs(new_col-old_col)==0){
        return true;
    } //move king horizontally
    else if (abs(new_row-old_row)==0 && abs(new_col-old_col)==1){
        return true;
    }
    //move king diagonally
    else if (abs(new_row-old_row)==1 && abs(new_col-old_col)==1){
        return true;
    }
    else{
        return false;
    }
}

// Check that it is your turn
// Author Thorbjoern
bool Chess::your_turn(std::vector<int> pos)
{
    if((board[pos[0]][pos[1]]>6) && ((turns%2)==1))
    {
        return true;
    }
    else if((board[pos[0]][pos[1]]<7 && board[pos[0]][pos[1]]>0)  && ((turns%2)==0))
    {
        return true;
    }
    else
    {
        return false;
    }
}

// This functions checks what type of piece we are trying to move and if it is a valid move
// Also ensures the players alternates taking turns
// Author Sam, refactored and integrated by Thorbjoern
bool Chess::move_piece(std::vector<int> pos){     
    // color is a flag for ensuring pieces move correctly, 0=white, 1=black
    int color;
    // First we check that the correct player is trying to make a move.
    if(board[pos[0]][pos[1]]>6) // && ((turns%2)==1))
        color = 1;
    else if(board[pos[0]][pos[1]]<7 && board[pos[0]][pos[1]]>0) // && ((turns%2)==0))
        color = 0;
    else
    {
        return false;
    }
    switch (board[pos[0]][pos[1]]%6){
        case 1:     //move pawn
            return move_pawn(pos, color);
            break;
        case 2:     //move rook
            return move_rook(pos);
            break;
        case 3:     //move knight
            return move_knight(pos);
            break;
        case 4:     //move bishop
            return move_bishop(pos);
            break;
        case 5:     //move queen
            return move_queen(pos);
            break;
        case 0:     //move king
            return move_king(pos);
            break;
        default:
            return false;
    }
}

// Display all the moves that happened on the board.
// Author Thorbjoern
void Chess::print_moves()
{
    int N = moves.size();
    std::vector<std::string> letters = {"a","b","c","d","e","f","g","h"};
    for(int i=0;i<N;i++)
    {
        std::cout<<letters[moves[i][1]]<<moves[i][0]+1<<" "<<letters[moves[i][3]]<<moves[i][2]+1<<std::endl;
    }
}

// Construct the board matrix
// Author Thorbjoern
void Chess::setup_board()
{
    std::vector<std::vector<int> > chess_board = {
      {  2,    3,    4,    5,    6,    4,    3,    2 },
      {  1,    1,    1,    1,    1,    1,    1,    1 },
      {  0,    0,    0,    0,    0,    0,    0,    0 },
      {  0,    0,    0,    0,    0,    0,    0,    0 },
      {  0,    0,    0,    0,    0,    0,    0,    0 },
      {  0,    0,    0,    0,    0,    0,    0,    0 },
      {  7,    7,    7,    7,    7,    7,    7,    7 },
      {  8,    9,   10,   11,   12,   10,    9,    8 },
    };
    this->board=chess_board;
    turns = 0;
}

// Reset the game to initial configuration
// Authort Thorbjoern
void Chess::reset_board()
{
    turns = 0;
    setup_board();
    std::vector<std::vector<int>> moves_cleared;
    moves = moves_cleared;
}

// Redo the moves found in the moves list
// Author Thorbjoern
void Chess::reset_from_moves()
{
    setup_board();
    for(std::vector<int> a_move : moves)
    {
        move(a_move);
        moves.pop_back(); // Since we already have the moves in the list.
    } 
}

// Draw the board in the console
// Author Thorbjoern
void Chess::draw_chessboard() 
{
    std::cout<<"   a b c d e f g h"<<std::endl;
    for (int height = 0; height < 8; height++)
    {
        for (int width = 0; width < 8; width++)
        {
            if(width == 0)
            {
                std::cout<<(height+1)<<" ";
            }
            if(((height+width)%2)==1)
            {
                std::cout<<"\x1B[48;5;8m";
            }
            else
            {
                std::cout<<"\x1B[48;5;0m";
            }
            if(board[height][width]>0)
            {
                std::cout<<""<<print_chars[board[height][width]-1]<<" ";
            }
            else
            {
                std::cout<<"  ";
            }
        }
    std::cout<<"\x1B[48;5;0m"<<std::endl;
    }
}

// Serialize the moves list and save it to a save slot
// Author Thorbjoern
void Chess::save_board() 
{
    std::cout<<"Choose save slot [0, 9]: ";
    std::string input, file_name; 
    getline(std::cin, input);
    if((int(input[0])-49)>=0 && (int(input[0])-49)<10)
    {
        file_name = "save_"+input+".dat";
    }
    else
    {
        file_name = "save_0.dat";
    }
    std::string serialized = hps::to_string(moves);
    std::cout << "Writing " << serialized.size() << " Bytes" << std::endl;
    std::ofstream dataFile;
    dataFile.open(file_name, std::ios::binary); // | std::ios::app);
    dataFile << serialized <<std::endl;
    dataFile.close();
}

// Load a serialized save point
// Author Thorbjoern
void Chess::load_board()
{
    std::cout<<"Choose save slot [0, 9]: ";
    std::string input, file_name; 
    getline(std::cin, input);
    if((int(input[0])-49)>=0 && (int(input[0])-49)<10)
    {
    }
    else
    {
        input = "0";
    }
    file_name = "save_"+input+".dat";

    std::cout<<"Loading save "+input<<std::endl;

    std::vector<std::vector<int>> moves_loaded = {{0,0,0,0}};
    moves = moves_loaded;

    std::string line;

    std::ifstream dataFile;

    dataFile.open(file_name, std::ios::binary);
    if(dataFile.is_open())
    {
        getline(dataFile, line);
        moves_loaded = hps::from_string<std::vector<std::vector<int>>>(line);
        dataFile.close();
        std::cout<<"Loading complete"<<std::endl;
        moves = moves_loaded;
    }
    else
    {
        std::cout << "Loading failed" <<std::endl;
    }
}

// Check if there is any piece in between the start and the end of the move.
// Initial version Hannes, optimized by Thorbjoern
bool Chess::check_blocked(std::vector<int> pos)
{
    // If at least one tile between the start and end
    if(abs(pos[0]-pos[2])>1 || abs(pos[1]-pos[3])>1)
    {
        int sum = 0;
        int N = std::max(std::abs(pos[2]-pos[0]),std::abs(pos[3]-pos[1]));
        int dx = (pos[2]-pos[0])/N;
        int dy = (pos[3]-pos[1])/N;
        for(int i=1; i<N;i++)
        {
            sum += board[pos[0]+i*dx][pos[1]+i*dy];
        }
        if(sum>0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
    else
    {
        return true;
    }
}

// Checks whether there is a piece in the position that is passed in
// Author Hannes
bool Chess::positionOccupied(int row, int column)
{
   bool is_occupied = false;

   if ( 0 != getValuePosition(row, column))//0 resembles empty position on board
   {
      is_occupied = true;
   }

   return is_occupied;
}

// Return value of the position on the board
// Author Hannes
int Chess::getValuePosition(int row, int column)
{
   return board[row][column];
}

// Locate the tile with the white king on the board
// Author Thorbjoern
Chess::Position Chess::find_white_king()
{
    Position king_pos;
    for(int i=0;i<8;i++)
    {
        for(int j=0;j<8;j++)
        {
            if(board[i][j]==6)
            {
                king_pos.row = i;
                king_pos.column = j;
                return king_pos;
            }
        }
    }
    king_pos.row = -1;
    king_pos.column = -1;
    return king_pos;
}

// Locate the tile with the black king on the board
// Author Thorbjoern
Chess::Position Chess::find_black_king()
{
    Position king_pos;
    for(int i=0;i<8;i++)
    {
        for(int j=0;j<8;j++)
        {
            if(board[i][j]==12)
            {
                king_pos.row = i;
                king_pos.column = j;
                return king_pos;
            }
        }
    }
    king_pos.row = -1;
    king_pos.column = -1;
    return king_pos;
}

// Check if white is checked
// Author Thorbjoern
bool Chess::is_white_checked()
{
    Position king = find_white_king();
    std::vector<int> pos;
    for(int i=0; i<8;i++)
    {
        for(int j=0;j<8;j++)
        {
            if(board[i][j]>6)
            {
                pos = {i,j,king.row,king.column};
                if((is_knight(pos) || check_blocked(pos)) && validate_move(pos) && move_piece(pos))
                {
                    return true;
                }
            }
        }
    }
    return false;
}

// Check if black is checked
// Author Thorbjoern
bool Chess::is_black_checked()
{
    Position king = find_black_king();
    std::vector<int> pos;
    for(int i=0; i<8;i++)
    {
        for(int j=0;j<8;j++)
        {
            if(board[i][j]>0 && board[i][j]<7)
            {
                pos = {i,j,king.row,king.column};
                if((is_knight(pos) || check_blocked(pos)) && validate_move(pos) && move_piece(pos))
                {
                    return true;
                }
            }
        }
    }
    return false;
}

/* bool Chess::isChecked(Position king_pos){

    bool check_mated;
    //Check if king can be attacked
    Attacked king_targeted = checkTargeted(king_pos);
    int count_attackers = 0;
    //count attackers
    for (auto attacker:king_targeted.attackers)
    {
        count_attackers++;
    }
    //If there is not attacker return false(King not in check)
    if (count_attackers==0)
    {
        return 0;
    }
    //If there is one attacker, if the attacker can be attacked then king is in check, but not check mate
    if (count_attackers==1)
    {   
        Attacked targeting_king = checkTargeted(king_targeted.attackers[0]);
        if(targeting_king.targeted){
            return true;
        }else{
            if (board[targeting_king.attackers[0].row][targeting_king.attackers[0].column] != 4 && board[targeting_king.attackers[0].row][targeting_king.attackers[0].column] != 10)
            {
                if (canBeBlocked(king_pos, targeting_king.attackers[0]))
                {
                    return true;
                }else{
                    check_mated = true;
                }
            }else{
                check_mated = true;
            }
        }
    }

    Position options_king[8] = {{1, 0}, {1, 1}, {1, -1}, {-1, -1},{-1, 1}, {-1, 0}, {0, 1}, {0, -1}};
    for (size_t i = 0; i < 8; i++)
    {
        int current_row = king_pos.row + options_king[i].row;
        int current_column = king_pos.column + options_king[i].column;

        if (current_row < 0 || current_row > 7 || current_column < 0 || current_column > 7)
        {
            continue; //position outside of board, go to next iteration
        }

        if (board[king_pos.row][king_pos.row] == 6)
        {
            if (board[current_row][current_column] == 0 || board[current_row][current_column] > 6)
            {
                int target_square = board[current_row][current_column];
                board[king_pos.row][king_pos.column] = 0;
                board[current_row][current_column] = 6;
                Attacked king_new_pos = checkTargeted({current_row,current_column});
                if (!king_new_pos.targeted)
                {
                    check_mated = false;
                }
                board[king_pos.row][king_pos.column] = 6;
                board[current_row][current_column] = target_square;
            }
        }else{
            if (board[current_row][current_column] < 6)
            {
                int target_square = board[current_row][current_column];
                board[king_pos.row][king_pos.column] = 0;
                board[current_row][current_column] = 12;
                Attacked king_new_pos = checkTargeted({current_row,current_column});
                if (!king_new_pos.targeted)
                {
                    check_mated = false;
                }
                board[king_pos.row][king_pos.column] = 6;
                board[current_row][current_column] = target_square;
            }
        }   
    }
    if (check_mated)
    {
        if (board[king_pos.row][king_pos.column] == 6)
        {
            w_check_mate = true;
        }else{
            b_check_mate = true;
        }
    }
    return true;
}

bool Chess::canBeBlocked(Position king, Position threat){

    if(abs(king.row-threat.row)>1 || abs(king.column - threat.column)>1)
    {
        int sum = 0;
        int N = std::max(std::abs(threat.row-king.row), std::abs(threat.column-king.column));
        int dx = (threat.row-king.row)/N;
        int dy = (threat.column-king.column)/N;
        for(int i=1; i<N;i++)
        {
            Attacked square_to_block = checkTargeted({king.row +(i*dx), king.column+(i*dy)});
            if (square_to_block.targeted)
            {
                return true;
            }
        }
    }
    return false;
}
//Determine if king is in check
Chess::Attacked Chess::checkTargeted(Position pos){
    Attacked targeted;
    
    if (getValuePosition(pos.row,pos.column)<=6 && getValuePosition(pos.row,pos.column) > 0)
    {
        targeted.color = 'w';
    }else if(getValuePosition(pos.row,pos.column)>6){
        targeted.color = 'b';
    }else{
        if (turns % 2 == 1)
        {
            targeted.color = 'w';
        }
    }
    
    int piece_column = pos.column;
    int piece_row = pos.row;
    
    //Look through columns to right
    for (size_t i = piece_column+1; i < 8; i++)
    {
        int right_piece = getValuePosition(piece_row, i);
        if (right_piece != 0)
        {
            if (targeted.color == 'w')
            {
                if (right_piece<=6)
                {
                    break;//Same color to the right
                }else
                {
                    if (right_piece == 8 ||right_piece == 11)
                    {
                        targeted.targeted = true;
                        Position attacker = {piece_row, i};
                        targeted.attackers.push_back(attacker);
                        break;
                    }else{
                        break;
                    }   
                }  
            }else{
                if (right_piece>6)
                {
                    break;//Same color to the right
                }else
                {
                    if (right_piece == 2 ||right_piece == 5)
                    {
                        targeted.targeted = true;
                        Position attacker = {piece_row, i};
                        targeted.attackers.push_back(attacker);
                        break;
                    }else{
                        break;
                    }
                }  
            } 
        }    
    }//Look through columns to right
    for (size_t i = piece_column-1; i <= 0; i--)
    {
        int left_piece = getValuePosition(piece_row, i);
        if (left_piece != 0)
        {
            if (targeted.color == 'w')
            {
                if (left_piece<=6)
                {
                    break;//Same color to the right
                }else
                {
                    if (left_piece == 8 ||left_piece == 11)
                    {
                        targeted.targeted = true;
                        Position attacker = {piece_row, i};
                        targeted.attackers.push_back(attacker);
                        break;
                    }else{
                        break;
                    }   
                }  
            }else{
                if (left_piece>6)
                {
                    break;//Same color to the right
                }else
                {
                    if (left_piece == 2 ||left_piece == 5)
                    {
                        targeted.targeted = true;
                        Position attacker = {piece_row, i};
                        targeted.attackers.push_back(attacker);
                        break;
                    }else{
                        break;
                    }
                }  
            } 
        } 
    }//Look through rows down
    for (size_t i = piece_row+1; i < 8; i++)
    {
        
        int down_piece = getValuePosition(i, piece_column);
        if (down_piece != 0)
        {
            if (targeted.color == 'w')
            {
                if (down_piece<=6)
                {
                    break;//Same color to the right
                }else
                {
                    if (down_piece == 8 ||down_piece == 11)//Values for rook and queen
                    {
                        targeted.targeted = true;
                        Position attacker = {i, piece_column};
                        targeted.attackers.push_back(attacker);
                        break;
                    }else{
                        break;
                    }   
                }  
            }else{
                if (down_piece>6)
                {
                    break;//Same color to the right
                }else
                {
                    if (down_piece == 2 ||down_piece == 5)//Values for white rook and queen
                    {
                        targeted.targeted = true;
                        Position attacker = {i, piece_column};
                        targeted.attackers.push_back(attacker);
                        break;
                    }else{
                        break;
                    }
                }  
            } 
        }
    }
    //Check through rows up
    for (size_t i = piece_row+1; i < 8; i++)
    {
        
        int up_piece = getValuePosition(i, piece_column);
        if (up_piece != 0)
        {
            if (targeted.color == 'w')
            {
                if (up_piece<=6)
                {
                    break;//Same color to the right
                }else
                {
                    if (up_piece == 8 ||up_piece == 11)//Values for rook and queen
                    {
                        targeted.targeted = true;
                        Position attacker = {i, piece_column};
                        targeted.attackers.push_back(attacker);
                        break;
                    }else{
                        break;
                    }   
                }  
            }else{
                if (up_piece>6)
                {
                    break;//Same color to the right
                }else
                {
                    if (up_piece == 2 ||up_piece == 5)//Values for white rook and queen
                    {
                        targeted.targeted = true;
                        Position attacker = {i, piece_column};
                        targeted.attackers.push_back(attacker);
                        break;
                    }else{
                        break;
                    }
                }  
            } 
        }
    }
    //Checking right down diagonal
    for (int i = piece_row + 1, j = piece_column + 1; i < 8 && j < 8; i++, j++){
        
        int dia_piece = getValuePosition(i, j);
        if (dia_piece != 0)
        {
            if (targeted.color == 'w')
            {
                if (dia_piece<=6)
                {
                    break;
                }else{
                    if (dia_piece == 9 || dia_piece == 11)
                    {
                        targeted.targeted = true;
                        Position attacker = {i, j};
                        targeted.attackers.push_back(attacker);
                        break;
                    }else{
                        break;
                    }
                }
            }else{
                if (dia_piece>6)
                {
                    break;//Same color to the right
                }else
                {
                    if (dia_piece == 3 ||dia_piece == 5)//Values for white rook and queen
                    {
                        targeted.targeted = true;
                        Position attacker = {i, j};
                        targeted.attackers.push_back(attacker);
                        break;
                    }else{
                        break;
                    }
                }  
            }
        }
    }
    //Checking left down diagonal
    for (int i = piece_row + 1, j = piece_column - 1; i < 8 && j > 0; i++, j--){
        
        int dia_piece = getValuePosition(i, j);
        if (dia_piece != 0)
        {
            if (targeted.color == 'w')
            {
                if (dia_piece<=6)
                {
                    break;
                }else{
                    if (dia_piece == 9 || dia_piece == 11)
                    {
                        targeted.targeted = true;
                        Position attacker = {i, j};
                        targeted.attackers.push_back(attacker);
                        break;
                    }else{
                        break;
                    }
                }
            }else{
                if (dia_piece>6)
                {
                    break;//Same color found
                }else
                {
                    if (dia_piece == 3 ||dia_piece == 5)//Values for white rook and queen
                    {
                        targeted.targeted = true;
                        Position attacker = {i, j};
                        targeted.attackers.push_back(attacker);
                        break;
                    }else{
                        break;
                    }
                }  
            }
        }
    }
    //Checking up right diagonal
    for (int i = piece_row - 1, j = piece_column + 1; i > 0 && j < 8; i--, j++){
        
        int dia_piece = getValuePosition(i, j);
        if (dia_piece != 0)
        {
            if (targeted.color == 'w')
            {
                if (dia_piece<=6)
                {
                    break;
                }else{
                    if (dia_piece == 9 || dia_piece == 11)
                    {
                        targeted.targeted = true;
                        Position attacker = {i, j};
                        targeted.attackers.push_back(attacker);
                        break;
                    }else{
                        break;
                    }
                }
            }else{
                if (dia_piece>6)
                {
                    break;//Same color to the right
                }else
                {
                    if (dia_piece == 2 ||dia_piece == 5)//Values for white rook and queen
                    {
                        targeted.targeted = true;
                        Position attacker = {i, j};
                        targeted.attackers.push_back(attacker);
                        break;
                    }else{
                        break;
                    }
                }  
            }
        }
    }
    //Checking up left diagonal
    for (int i = piece_row - 1, j = piece_column - 1; i > 0 && j > 0; i--, j--){
        
        int dia_piece = getValuePosition(i, j);
        if (dia_piece != 0)
        {
            if (targeted.color == 'w')
            {
                if (dia_piece<=6)
                {
                    break;
                }else{
                    if (dia_piece == 9 || dia_piece == 11)
                    {
                        targeted.targeted = true;
                        Position attacker = {i, j};
                        targeted.attackers.push_back(attacker);
                        break;
                    }else{
                        break;
                    }
                }
            }else{
                if (dia_piece>6)
                {
                    break;//Same color to the right
                }else
                {
                    if (dia_piece == 2 ||dia_piece == 5)//Values for white rook and queen
                    {
                        targeted.targeted = true;
                        Position attacker = {i, j};
                        targeted.attackers.push_back(attacker);
                        break;
                    }else{
                        break;
                    }
                }  
            }
        }
    }
    //King can be attacked from 8 angles by knights
    Position options_knight[8] = {{1,-2}, {1,2}, {-1, 2}, {-1, -2},{2, -1}, {2, 1}, {-2, 1}, {-2, -1}};

    //Iterate through the 8 options to see if a knight is reaching the king
    for (size_t i = 0; i < 8; i++)
    {
        
        //Set new coordinates to look for
        int current_row = piece_row + options_knight[i].row;
        int current_column = piece_column + options_knight[i].column;
        int piece = getValuePosition(current_row, current_column);
        if (current_row < 0 || current_row > 7 || current_column < 0 || current_column > 7)
        {
            continue; //position outside of board, go to next iteration
        }
        
        if (piece != 0)
        {
            if (targeted.color = 'w')
            {
                if (piece <=6)
                {
                    continue;
                }else{
                    if (piece == 10)
                    {
                        targeted.targeted = true;
                        Position attacker = {current_row, current_column};
                        targeted.attackers.push_back(attacker);
                        break;
                    }else{
                        continue;
                    }   
                }   
            }else{
                if (piece > 6)
                {
                    continue;
                }else{
                    if (piece == 4)
                    {
                        targeted.targeted = true;
                        break;
                    }   
                }   
            }   
        }
    }
    return targeted;
}
 */