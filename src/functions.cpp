#include "functions.h"

// Check if the input can be converted to a move. Returns an invalid move if not.
// Resulting in the a recall of this function with a new input.
// Author Thorbjoern
std::vector<int> get_move_from_user(std::string input)
{
    std::vector<int> pos(4);
    pos[1] = int(input[0])-97;
    pos[0] = int(input[1])-49;
    pos[3] = int(input[3])-97;
    pos[2] = int(input[4])-49;
    if((pos[0]<0)||(pos[0]>7)||(pos[1]<0)||(pos[1]>7)||(pos[2]<0)||(pos[2]>7)||(pos[3]<0)||(pos[3]>7))
    {
        std::vector<int> move(4);
        return move;
    }
    return pos;
}

// Prompt the current player to concede if wanting to quit
// Author Sam
bool concede_game(int turns){   //made this into a bool. can be called in main and its result saved in keep_playing variable
    start:     
    bool playerwantsconcede;
    std::string input;
    std::cout<< "Do you wish to concede? y/n "<< std::endl;
    getline(std::cin, input);
    if ((input=="y" || input=="Y") && (turns%2)==0){
        std::cout<<"White has conceded. Black wins "<< std::endl;
        return playerwantsconcede=0;
    }
    else if ((input=="y" || input=="Y") && (turns%2)==1){
        std::cout<<"Black has conceded. White wins "<< std::endl;
        return playerwantsconcede=0;
    }
    else if (input=="n" || input=="N") {
        return playerwantsconcede=1;
    } 
    else {
        std::cout<<"Invalid input "<< std::endl;
        goto start;
    }
}


