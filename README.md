# Assignment 2 - A PvP Chess Game
A two-player chess game based on standard rules.

## Table of Contents

- [Requirements](#requirements)
- [Setup](#setup)
- [Playing guide](#playing-guide)
- [Members' tasks](#members-tasks)
- [Maintainers](#maintainers)
- [Contributers](#contributers)

## Requirements

Requires `gcc` and `cmake`.

## Setup

    $ mkdir build
    $ cd build
    $ cmake ../
    $ make
    $ ./main

## Playing guide
To play, run the makefile and execute the main file. The game will prompt the user(s) with a set of options. To start a new game, press "n". To load a previously saved game, press "l". During the actual game, there are further options that can be accessed. To save a game, press "s". To concede a game, press "q". To print all moves that have occurred, press "p". The inputs are not case-sensitive.

Upon starting a new game, the program will display a chessboard. The first player will be prompted to enter a set of coordinates in the following format: (letter)(number) (letter)(number). An example of this is: d2 d4, where the first coordinate is the piece to be moved and the second coordinate is the destination of the current piece. After a piece has been moved, the moves are saved and the other player will be asked to move a piece. After a checkmate has been established, a winner is declared.

Example of play:

![chess](images/chess.JPG)

Example of saving a game:

![saving](images/saving.JPG)

Example of loading a saved game:

![loading](images/loading.JPG)

Example of being checked

![chess](images/incheck.JPG)

## Members' tasks
All functions in the source code are annotated with the author. Below is a list of the tasks completed by each member in the group.

**[Thorbjørn (@Thunderl3ear)](https://gitlab.com/Thunderl3ear):**

* Creating the README 
* Constructing the board
* Displaying the board
* Getting user input
* The game playing loop 
* Save/load functionality, board reset from loaded file.
* Parts of the validate move functions
* Pawn Promotion
* Castling

**[Hannes @HannesRingblom](https://gitlab.com/HannesRingblom):**

* Maintaining git repo and handling merges
* Initial check functionality
* Initial check functionality
* Block function 

**[Sam @sammpsys](https://gitlab.com/sammpsys):**

* Adding README description
* Move_piece function and movement functions for each piece
* Validate move function
* Concede game function

## Maintainers

[Hannes @HannesRingblom](https://gitlab.com/HannesRingblom)

## Contributers and License
Copyright 2022,

[Thorbjørn (@Thunderl3ear)](https://gitlab.com/Thunderl3ear)
[Hannes @HannesRingblom](https://gitlab.com/HannesRingblom)
[Sam @sammpsys](https://gitlab.com/sammpsys)

